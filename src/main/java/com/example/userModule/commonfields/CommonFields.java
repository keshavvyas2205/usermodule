package com.example.userModule.commonfields;

import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CommonFields {
	private long createdAt = System.currentTimeMillis();
	private long updatedAt = System.currentTimeMillis();
	private boolean isDeleted = false;
	private long deletedAt;

}
