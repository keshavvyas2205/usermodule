package com.example.userModule.requestDto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddUserCommunicatorDto {



	private long id;
	
	private String userName;
}
