package com.example.userModule.requestDto;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public class RegisterDto {
	private long id;
	@NotNull(message = "USERNAME SHOULD NOT BE NULL")
	@NotEmpty(message = "USERNAME SHOULD NOT BE EMPTY")
	private String userName;
	@NotNull(message = "USERPASSWORD SHOULD NOT BE NULL")
	@NotEmpty(message = "USERPASSWORD SHOULD NOT BE EMPTY")
	private String userPassword;
	@NotNull(message = "FIRSTNAME SHOULD NOT BE NULL")
	@NotEmpty(message = "FIRST SHOULD NOT BE EMPTY")
	private String firstName;
	@NotNull(message = "LASTNAME SHOULD NOT BE NULL")
	@NotEmpty(message = "LASTNAME SHOULD NOT BE EMPTY")
	private String lastName;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public RegisterDto() {
	}
	public RegisterDto(long id,
			@NotNull(message = "USERNAME SHOULD NOT BE NULL") @NotEmpty(message = "USERNAME SHOULD NOT BE EMPTY") String userName,
			@NotNull(message = "USERPASSWORD SHOULD NOT BE NULL") @NotEmpty(message = "USERPASSWORD SHOULD NOT BE EMPTY") String userPassword,
			@NotNull(message = "FIRSTNAME SHOULD NOT BE NULL") @NotEmpty(message = "FIRST SHOULD NOT BE EMPTY") String firstName,
			@NotNull(message = "LASTNAME SHOULD NOT BE NULL") @NotEmpty(message = "LASTNAME SHOULD NOT BE EMPTY") String lastName) {
		this.id = id;
		this.userName = userName;
		this.userPassword = userPassword;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
}
      
      
    

         