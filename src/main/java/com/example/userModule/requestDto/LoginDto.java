package com.example.userModule.requestDto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LoginDto {
    @NotNull(message = "username can not be null")
	@NotEmpty(message = "username can not be empty")
	String userName;
	@NotEmpty(message = "password can not be null")
	@NotNull(message = "password can not be empty")
	String password;
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public LoginDto(
            @NotNull(message = "username can not be null") @NotEmpty(message = "username can not be empty") String userName,
            @NotEmpty(message = "password can not be null") @NotNull(message = "password can not be empty") String password) {
        this.userName = userName;
        this.password = password;
    }
    public LoginDto() {
    }
    
}
