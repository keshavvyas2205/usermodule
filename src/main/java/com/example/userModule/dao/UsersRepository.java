package com.example.userModule.dao;

import com.example.userModule.models.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface UsersRepository extends JpaRepository<Users,Long> {

	Users findByUserNameIgnoreCase(String userName);
    
}
