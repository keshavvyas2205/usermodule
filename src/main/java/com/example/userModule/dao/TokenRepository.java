package com.example.userModule.dao;

import com.example.userModule.models.Token;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface TokenRepository extends JpaRepository<Token,Long>{

	Token findByUserToken(String userToken);
    
}
