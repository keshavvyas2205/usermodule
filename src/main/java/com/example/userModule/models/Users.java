package com.example.userModule.models;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import com.example.userModule.commonfields.CommonFields;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
public class Users  extends CommonFields{
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Id
	@GeneratedValue(generator = "user_generator")
	@SequenceGenerator(
			name = "user_generator",
			sequenceName = "user_sequence",
			initialValue = 100000
			)
	private long id;
	private String userName;
	private String userPassword;
	private String firstName;
	private String lastName;
	public Users(long id, String userName, String userPassword, String firstName, String lastName) {
		super();
		this.id = id;
		this.userName = userName;
		this.userPassword = userPassword;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	public Users() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Users(long createdAt, long updatedAt, boolean isDeleted, long deletedAt) {
		super(createdAt, updatedAt, isDeleted, deletedAt);
		// TODO Auto-generated constructor stub
	}
	
}
