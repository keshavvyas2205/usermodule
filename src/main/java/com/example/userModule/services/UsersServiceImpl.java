package com.example.userModule.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.sql.Date;

import com.example.userModule.dao.TokenRepository;
import com.example.userModule.dao.UsersRepository;
import com.example.userModule.httpResponse.DataResponse;
import com.example.userModule.httpResponse.Messages;
import com.example.userModule.httpResponse.RestResponse;
import com.example.userModule.httpResponse.StatusCodes;
import com.example.userModule.models.Token;
import com.example.userModule.models.Users;
import com.example.userModule.requestDto.LoginDto;
import com.example.userModule.requestDto.LoginReturnDtos;
import com.example.userModule.requestDto.RegisterDto;
@Service
public class UsersServiceImpl implements UsersService {
	
	@Autowired
	TokenRepository tokenRepository;
	
	@Autowired
	UsersRepository usersRepository;

	

	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	CommunicatorService communicatorService;

	@Override
	public RestResponse registerUser(RegisterDto registerDto) {

		Users u = null;
		u = usersRepository.findByUserNameIgnoreCase(registerDto.getUserName());
		if (u != null) {
			return new DataResponse(StatusCodes.ALREADY_EXISTS, Messages.USER_NAME_ALREADY_EXISTS, null);
		}
		String password = bCryptPasswordEncoder.encode(registerDto.getUserPassword());
		u = modelMapper.map(registerDto, Users.class);
		u.setUserPassword(password);
		Users savedUser = usersRepository.save(u);
		communicatorService.addUser(savedUser.getId(),savedUser.getUserName());
		return new DataResponse(StatusCodes.SUCCESS, Messages.SIGN_UP_SUCESSFULLY_VERIFY_NOW, u);
	}

	@Override
	public RestResponse login(LoginDto loginDto) {

		Users u = null;
		u = usersRepository.findByUserNameIgnoreCase(loginDto.getUserName());
		if (u == null) {
				return new DataResponse(StatusCodes.INVALID_CREDENTIALS_STATUS, Messages.USER_NOT_FOUND, null);
		}
		if (!(bCryptPasswordEncoder.matches(loginDto.getPassword(), u.getUserPassword()))) {
			return new DataResponse(StatusCodes.INVALID_CREDENTIALS_STATUS, Messages.INCORRECT_PASSWORD, null);
		}
		Token token = new Token();
		token.setUserId(u.getId());
		token.setUserToken(this.getToken(u.getId()));
		tokenRepository.save(token);
		LoginReturnDtos loginReturnDto = new LoginReturnDtos(u.getFirstName(), u.getLastName(),
				token.getUserToken());
		System.out.println(loginReturnDto);
		return new DataResponse(StatusCodes.SUCCESS, Messages.LOGIN_SUCESSFULLY, loginReturnDto);
	}

	private String getToken(long id) {
		String userId = "" + id;
		String token = Jwts.builder()
				.setSubject(userId)
				.setExpiration(new Date(System.currentTimeMillis() + 100_000_000))
				.signWith(SignatureAlgorithm.HS512, "MustBeUniqueEverwhere")
				.compact();
		return token;
	}

	@Override
	public RestResponse logout(String userToken) {
		Token token = tokenRepository.findByUserToken(userToken);
		System.out.println(token);
		tokenRepository.delete(token);
		return new DataResponse(StatusCodes.SUCCESS, "token Deleted", null);
	}
}
