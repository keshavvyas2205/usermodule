package com.example.userModule.services;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;

import com.example.userModule.httpResponse.DataResponse;

public interface CommunicatorService {

	ResponseEntity<DataResponse> addUser(long id, String userName);

	 ResponseEntity<DataResponse> getUsersList(int offset, int pageSize);

	 ResponseEntity<DataResponse> getChats(long fromUserId, long toUserId, int offset, int pageSize);
	
}
