package com.example.userModule.services;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.example.userModule.httpResponse.DataResponse;
import com.example.userModule.httpResponse.Messages;
import com.example.userModule.httpResponse.StatusCodes;
import com.example.userModule.requestDto.AddUserCommunicatorDto;

@Service
public class CommunicatorServiceImpl implements CommunicatorService {

	@Autowired
	private RestTemplate restTemplate;

	@Override
	public ResponseEntity<DataResponse> addUser(long id, String userName) {
		AddUserCommunicatorDto addUserCommunicatorDto = new AddUserCommunicatorDto(id, userName);
		try {
			HttpEntity<AddUserCommunicatorDto> req = new HttpEntity<>(addUserCommunicatorDto);
			ResponseEntity<DataResponse> res = restTemplate.exchange("http://localhost:8080/user/registeruser",
					HttpMethod.POST, req, DataResponse.class);
			System.out.println(res);
			return res;
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(new DataResponse(500,"SERVER ERROR",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Override
	public  ResponseEntity<DataResponse> getUsersList(int offset, int pageSize) {
		try {
			ResponseEntity<DataResponse> res = restTemplate.exchange(
					"http://localhost:8080/user/getuserslist/" + offset + "/" + pageSize + "", HttpMethod.GET, null,
					DataResponse.class);
			return res;
		} catch (Exception e) {
			return new ResponseEntity<>(new DataResponse(500,"SERVER ERROR",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@Override
	public ResponseEntity<DataResponse> getChats(long fromUserId, long toUserId, int offset, int pageSize) {

		try {
			ResponseEntity<DataResponse> res = restTemplate.exchange("http://localhost:8080/user/getchats/" + fromUserId + "/"
					+ toUserId + "/" + offset + "/" + pageSize + "", HttpMethod.GET, null, DataResponse.class);
			
			return res;

		} catch (Exception e) {
			return new ResponseEntity<>(new DataResponse(500,"SERVER ERROR",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
