package com.example.userModule.services;

import org.springframework.http.ResponseEntity;

import com.example.userModule.httpResponse.RestResponse;
import com.example.userModule.models.Users;
import com.example.userModule.requestDto.LoginDto;
import com.example.userModule.requestDto.RegisterDto;

public interface UsersService {

	RestResponse registerUser(RegisterDto registerDto);

	RestResponse login(LoginDto loginDto);

	RestResponse logout(String token);
	
}
