package com.example.userModule.controller;

import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.userModule.httpResponse.DataResponse;
import com.example.userModule.httpResponse.StatusCodes;
import com.example.userModule.services.CommunicatorService;



@RequestMapping("/users")
@RestController
public class ChatController {
	@Autowired
	CommunicatorService communicatorService;
	@GetMapping("/getuserslist/{offset}/{pageNo}")
	public ResponseEntity<DataResponse> getUsersList(@PathVariable("offset") int offset,@PathVariable("pageNo") int pageSize)
	{
		if(offset<1)
		{
			return new ResponseEntity<>(new DataResponse(400,"PageNo cannot Be Less Than One",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(pageSize<1)
		{
			return new ResponseEntity<>(new DataResponse(400,"PageSize can not be less than one",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try
		{
			return communicatorService.getUsersList(offset,pageSize);	
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(new DataResponse(500,"SERVER ERROR",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
	}
	
	@GetMapping("/getchats/{fromUserId}/{toUserId}/{offset}/{pageNo}")
	public ResponseEntity<DataResponse> getChats(@PathVariable("fromUserId") long fromUserId, @PathVariable("toUserId") long toUserId,@PathVariable("offset") int offset,@PathVariable("pageNo") int pageSize)
	{
		if(offset<1)
		{
			return new ResponseEntity<>(new DataResponse(400,"PageNo cannot Be Less Than One",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(pageSize<1)
		{
			return new ResponseEntity<>(new DataResponse(400,"PageSize can not be less than one",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	
		try
		{
			return communicatorService.getChats(fromUserId,toUserId,offset,pageSize);
		}
		catch(Exception e)
		{
			return new ResponseEntity<>(new DataResponse(500,"SERVER ERROR",null),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}


