package com.example.userModule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.example.userModule.httpResponse.DataResponse;
import com.example.userModule.httpResponse.RestResponse;
import com.example.userModule.models.Users;
import com.example.userModule.requestDto.LoginDto;
import com.example.userModule.requestDto.RegisterDto;
import com.example.userModule.services.UsersService;

@RequestMapping("/users")
@RestController
public class UserController {

	@Autowired
	UsersService usersService;

	@PostMapping("/registeruser")
	public RestResponse registerUser(@Valid @RequestBody RegisterDto registerDto, BindingResult result) {

		try {
			if (result.getAllErrors() != null && !result.getAllErrors().isEmpty())
				return new DataResponse(400, result.getAllErrors().get(0).getDefaultMessage(), null);
			return usersService.registerUser(registerDto);
		} catch (Exception e) {
			return new DataResponse(500, e.getMessage(), null);
		}
	}

	@PostMapping("/login")
	public RestResponse login(@Valid @RequestBody LoginDto loginDto, BindingResult result) {

		try {
			if (result.getAllErrors() != null && !result.getAllErrors().isEmpty())
				return new DataResponse(400, result.getAllErrors().get(0).getDefaultMessage(), null);
			return usersService.login(loginDto);
		} catch (Exception e) {
			return new DataResponse(500, e.getMessage(), null);
		}
	}

	@DeleteMapping("/logout")
	public RestResponse logout(HttpServletRequest req) {
		try {
			return usersService.logout(req.getHeader("Authorization"));
		} catch (Exception e) {
			return new DataResponse(500, e.getMessage(), null);
		}
	}

}
